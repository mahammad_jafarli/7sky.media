<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    public static function socials()
    {
        return static::orderBy('id', 'DESC')->get();
    }

    protected $fillable = [
        'icon','link'
    ];
}
