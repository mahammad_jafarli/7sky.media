<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $fillable = [
        "title_ru",
        "title_az",
        "text_en",
        "text_ru",
        "text_az",
        "keywords",
        "video",
        "image",
    ];
}
