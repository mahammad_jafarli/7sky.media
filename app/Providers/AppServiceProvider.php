<?php

namespace App\Providers;

use App\Category;
use App\Contact;
use App\Post;
use App\Social;
use App\Tag;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        DB::listen(function ($sql){
            error_log('Url: '. \Request::fullUrl().' Query: '.$sql->sql);
        });

        view()->composer('frontend.includes.topNavbar', function($view){
            $view->with('socials', Social::socials());
        });

        view()->composer('frontend.includes.footer', function($view){
            $view->with('socials', Social::socials());
            $view->with('postsSlider', Post::postsSlider());
        });

        view()->composer('frontend.includes.navbar', function($view){
            $view->with('categories', Category::categories());
        });

        view()->composer('frontend.includes.rightSidebar', function($view){
            $view->with('postsTrend', Post::postsTrend());
            $view->with('postsSlider', Post::postsSlider());
            $view->with('tags', Tag::tags());
        });
        view()->composer('frontend.includes.leftSidebar', function($view){
            $view->with('postsTrend', Post::postsTrend());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
