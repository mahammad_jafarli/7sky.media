<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    public static function contact()
    {
        return static::first();
    }

    protected $fillable = [
        'phone','email','address', 'location'
    ];
}
