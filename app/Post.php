<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function tags(){
        return $this->belongsToMany('App\Tag','post_tags');
    }

    public function images(){
        return $this->hasMany('App\Images', 'post_image_id');
    }

    public function comment(){
        return $this->hasMany('App\Comment', 'post_id');
    }

    public static function postsTrend()
    {
        return static::orderBy('count', 'DESC')->take(3)->get();
    }

    public static function postsSlider()
    {
        return static::orderBy('id', 'DESC')->take(3)->get();
    }

    protected $fillable = [
        'name_ru', 'name_az', 'text_ru', 'text_az', 'count', 'category_id', 'keywords', 'image'
    ];
}
