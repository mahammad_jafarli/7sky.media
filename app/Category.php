<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function post()
    {
        return $this->hasMany('App\Post', 'category_id');
    }

    public function parent_category()
    {
        return $this->hasMany('App\Category', 'parent_id');
    }

    public static function categories()
    {
        return static::orderBy('id', 'DESC')->get();
    }

    protected $fillable = [
        'name_ru', 'name_az', 'parent_id'
    ];
}
