<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class BanMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$redirectTo = '/')
    {
        if (!Auth::user()->status == 1) {
            Auth::logout();
            return redirect($redirectTo);
        }
        return $next($request);
    }
}
