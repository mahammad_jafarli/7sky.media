<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SocialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'link' => 'required',
            'icon' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'link.required' => 'Zəhmət olmasa Linki daxil edin',
            'icon.required' => 'Zəhmət olmasa İconu daxil edin',
        ];
    }
}
