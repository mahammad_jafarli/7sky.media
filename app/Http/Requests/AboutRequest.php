<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AboutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_az' => 'required',
            'title_ru' => 'required',
            'text_az' => 'required',
            'text_ru' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title_az.required' => 'Zəhmət olmasa Aze dilində başlığı daxil edin',
            'title_ry.required' => 'Zəhmət olmasa Rus dilində başlığı daxil edin',
            'text_az.required' => 'Zəhmət olmasa Aze dilində mətni daxil edin',
            'text_ru.required' => 'Zəhmət olmasa Rus dilində mətni daxil edin',
        ];
    }
}
