<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_ru' => 'required',
            'name_az' => 'required',
            'text_ru' => 'required',
            'text_az' => 'required',
            'category_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name_ru.required' => 'Zəhmət olmasa postun Rus dilində başlığını daxil edin',
            'name_az.required' => 'Zəhmət olmasa postun Aze dilində başlığını daxil edin',
            'text_ru.required' => 'Zəhmət olmasa postun Rus dilində mətnini daxil edin',
            'text_az.required' => 'Zəhmət olmasa postun Aze dilində mətnini daxil edin',
            'category_id.required' => 'Zəhmət olmasa postun kateqoriyasını seçin',
        ];
    }
}
