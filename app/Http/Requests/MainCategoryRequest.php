<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MainCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_az' => 'required',
            'name_ru' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name_az.required' => 'Zəhmət olmasa kateqoriyanin Aze dilinde adını daxil edin',
            'name_ru.required' => 'Zəhmət olmasa kateqoriyanin Rus dilinde adını daxil edin',
        ];
    }
}
