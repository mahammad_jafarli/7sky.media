<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_az' => 'required',
            'name_ru' => 'required',
            'parent_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name_az.required' => 'Zəhmət olmasa kateqoriyanın Aze dilinde adını daxil edin',
            'name_ru.required' => 'Zəhmət olmasa kateqoriyanın Rus dilinde adını daxil edin',
            'parent_id.required' => 'Zəhmət olmasa əsas kateqoriyanı seçin'
        ];
    }
}
