<?php

namespace App\Http\Controllers\Backend;
use App\Contact;
use App\Http\Requests\ContactRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contact = Contact::first();
        $settings = $this->getSettingsForTable();
        return view('admin.pages.contact.contact',compact('contact','settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        $setting = $this->getSettingsForForm();
        $setting['title'] = 'Yenilə';
        return view('admin.pages.contact.edit',compact('contact','setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ContactRequest $request, Contact $contact)
    {
        $contact->update($request->all());
        $request->session()->flash(str_slug('Yenilə','-'),'Əlaqələr yeniləndi');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getSettingsForTable()
    {
        return  [
            'title' => 'Əlaqələr',
            'table' => 'contacts',
            'createButton' => [
                'text' => "Əlaqələr",
                'url' => route('contact.index')
            ],
            'columns' => [
                [
                    'label' => 'ID',
                ],
                [
                    'label' => 'Ünvan',
                ],
                [
                    'label' => 'Telefon',
                ],
                [
                    'label' => 'Email',
                ]
            ],
        ];
    }

    /**
     * Return setting array for form component
     * @return array
     */
    private  function  getSettingsForForm()
    {
        return [
            'title' => 'Create category',
            'flashSessionKey' => 'category',
            'flashSessionValue' => 'Category created',
            'backButton' => [
                'text' => "Back",
                'url' => route('contact.index')
            ]
        ];
    }
}
