<?php

namespace App\Http\Controllers\Backend;

use App\Category;
use App\Comment;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(){
        $users = User::all();
        $posts = Post::all();
        $comments = Comment::all();
        $categories = Category::all();
        return view('admin.pages.index', compact('users', 'posts', 'comments', 'categories'));
    }
}
