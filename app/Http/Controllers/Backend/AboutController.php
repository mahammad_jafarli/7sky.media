<?php

namespace App\Http\Controllers\Backend;
use App\About;
use App\Http\Requests\AboutRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $about = About::first();
        $settings = $this->getSettingsForTable();
        return view('admin.pages.about.about', compact('settings', 'about'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(About $about)
    {
        $settings = $this->getSettingsForForm();
        $settings['title'] = 'Edit About';
        return view('admin.pages.about.edit', compact('settings', 'about', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AboutRequest $request, About $about)
    {
        if ($request->hasFile('img')) {
            $name = time() . "." . $request->file("img")->extension();
            $about->image = $name;
            $request->file("img")->move(public_path() . '/images/about', $name);
        }
        $about->update($request->all());
        $request->session()->flash(str_slug('Edit about','-'),'About edited');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getSettingsForTable()
    {
        return [
            'title' => 'Haqqımızda',
            'table' => 'about',
            'createButton' => [
                'text' => 'Haqqımızda',
                'url' => route('about.index')
            ],
            'columns' => [
                [
                    'label' => 'ID',
                ],
                [
                    'label' => 'Başlıq',
                ],
                [
                    'label' => 'Mətn',
                ],
                [
                    'label' => 'Şəkil',
                ]
            ],
        ];
    }

    private function getSettingsForForm()
    {
        return [
            'title' => 'Edit About',
            'flashSessionKey' => 'about',
            'flashSessionValue' => 'About Edit',
            'backButton' => [
                'text' => 'Back',
                'url' => route('about.index')
            ]
        ];
    }
}
