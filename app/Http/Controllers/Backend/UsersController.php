<?php

namespace App\Http\Controllers\Backend;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('id', 'DESC')->get();
        $settings = $this->getSettingsForTable();
        return view('admin.pages.user.users', compact('users', 'settings'));
    }

    public function setStatus(Request $request)
    {
        if ($request->ajax())
        {
            DB::table($request->table)->where('id',$request->id)->update(['status' => $request->status]);
            $resultMessage = ($request->status) ? "İstifadəçi aktiv olundu" : "İstifadəçi deaktiv olundu";
            return response($resultMessage,200);

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $setting = $this->getSettingsForForm();
        $setting['title'] = 'Düzəliş et';
        return view('admin.pages.user.edit',compact('user','setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();
        $request->session()->flash(str_slug('Düzəliş et','-'),'Düzəliş edildi');
        return back();
    }

        /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,User $user)
    {
        $user->delete();
        $request->session()->flash(str_slug('İstifadəçilər','-'),'İstifadəçi silindi');
        return back();
    }

    /**
     *  Return setting array for table component
     * @return array
     */
    private function getSettingsForTable()
    {
        return  [
            'title' => 'İstifadəçilər',
            'table' => 'users',
            'createButton' => [
                'text' => "İstifadəçi",
                'url' => route('user.index')
            ],
            'columns' => [
                [
                    'label' => 'ID',
                ],
                [
                    'label' => 'Ad',
                ],
                [
                    'label' => 'Email'
                ],
                [
                    'label' => 'Balans'
                ],
                [
                    'label' => 'Status',
                ]
            ],
        ];
    }

    /**
     * Return setting array for form component
     * @return array
     */
    private  function  getSettingsForForm()
    {
        return [
            'title' => 'Əlavə et',
            'flashSessionKey' => 'user',
            'flashSessionValue' => 'Əlavə edildi',
            'backButton' => [
                'text' => "Əvvələ",
                'url' => route('user.index')
            ]
        ];
    }
}
