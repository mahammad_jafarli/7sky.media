<?php

namespace App\Http\Controllers\Backend;
use App\Http\Requests\SocialRequest;
use App\Social;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SocialsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $socials = Social::orderBy('id', 'DESC')->get();
        $settings = $this->getSettingsForTable();
        return view('admin.pages.social.socials', compact('socials', 'settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $setting = $this->getSettingsForForm();
        return view('admin.pages.social.create',compact('setting'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SocialRequest $request, Social $socials)
    {
        $socials->create($request->all());
        $request->session()->flash(str_slug('Əlavə et','-'),'Əlavə edildi');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

        /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Social $social)
    {
        $setting = $this->getSettingsForForm();
        $setting['title'] = 'Düzəliş et';
        return view('admin.pages.social.edit',compact('social','setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SocialRequest $request, Social $social)
    {
        $social->update($request->all());
        $request->session()->flash(str_slug('Düzəliş et','-'),'Düzəliş edildi');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Social $social)
    {
        $social->delete();
        $request->session()->flash(str_slug('Soşial şəbəkələr','-'),'Silindi');
        return back();
    }

    /**
     *  Return setting array for table component
     * @return array
     */
    private function getSettingsForTable()
    {
        return  [
            'title' => 'Soşial şəbələr',
            'table' => 'socials',
            'createButton' => [
                'text' => "Əlavə et",
                'url' => route('socials.create')
            ],
            'columns' => [
                [
                    'label' => 'ID',
                ],
                [
                    'label' => 'Link',
                ],
                [
                    'label' => 'Icon',
                ]
            ],
        ];
    }

    /**
     * Return setting array for form component
     * @return array
     */
    private  function  getSettingsForForm()
    {
        return [
            'title' => 'Əlavə et',
            'flashSessionKey' => 'social',
            'flashSessionValue' => 'Əlavə edildi',
            'backButton' => [
                'text' => "Əvvələ",
                'url' => route('socials.index')
            ]
        ];
    }
}
