<?php

namespace App\Http\Controllers\Backend;

use App\Category;
use App\Images;
use App\MainCategory;
use App\Post;
use App\PostTag;
use App\Subscribe;
use App\Tag;
use App\Http\Requests\PostRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\ImageManagerStatic as Image;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('id', 'DESC')->get();
        $settings = $this->getSettingsForTable();
        return view('admin.pages.post.posts', compact('settings', 'posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::orderBy('id', 'DESC')->get();
        $tags = Tag::orderBy('id', 'DESC')->get();
        $settings = $this->getSettingsForForm();
        return view('admin.pages.post.create', compact('categories', 'settings', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function getSubCategoryById($categoryId,Request $request)
    {
        $result = [];
        $selectedCategory = Category::find($categoryId);
        $categories = Category::where('parent_id','=',$categoryId)->get();
        if (isset($categories) && $request->ajax())
        {
            if (!$categories->isEmpty())
            {
                $result['success'] = true;

                $result['category'] = $selectedCategory->id;

                $result['data'] = $categories;
            }
            else
            {
                $result['success'] = false;

                $result['category'] = $selectedCategory->id;

                $result['data'] = false;
            }
        }
        else
        {
            $result['success'] = false;
            $result['message'] = 'Category not found';
        }

        return response()->json($result,200);
    }

    public function store(PostRequest $request, Post $post)
    {
        if ($request->hasFile('image')) {
            $image = Image::make($request->file('image'));
            $orginalImageName = str_slug($request->name_az,'-').'.'.$request->file('image')->getClientOriginalExtension();
            $image->save(public_path('images/posts/'.$orginalImageName));
            $tumbnailImageName = rand(). ".".$orginalImageName;
            $image->fit(750, 500, function ($constraint) {
                $constraint->upsize();
            });
            $image->save(public_path('images/posts/'.$tumbnailImageName));
            $post->image = $tumbnailImageName;
        }

        $post->name_az = $request->name_az;
        $post->name_ru = $request->name_ru;
        $post->text_az = $request->text_az;
        $post->text_ru = $request->text_ru;
        $post->keywords = $request->keywords;
        $post->category_id = $request->category_id;
        $post->count = 0;
        $post->save();
        $post->tags()->sync($request->tags, false);
       $subscribes = Subscribe::all();
       foreach ($subscribes as $subscribe){
           $data = array(
               'post' => $post,
           );
           Mail::send('email.subscribe', $data, function($message) use ($subscribe) {
               $message->to($subscribe->email, '7sky')
                   ->subject("7 SKY");
               $message->from("info@7sky.media","7sky.media");
           });
       }
        $request->session()->flash(str_slug('Create post','-'),'Xəbər əlavə olundu');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        $settings['title'] = $post->name_ru;
        return view('admin.pages.post.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $settings = $this->getSettingsForForm();
        $settings['title'] = 'Edit post';
        $categories = Category::orderBy('id', 'DESC')->get();
        $tags = Tag::orderBy('id', 'DESC')->get();
        return view('admin.pages.post.edit',compact('categories','settings', 'post', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, Post $post)
    {
        if ($request->hasFile('image')) {
            $image = Image::make($request->file('image'));
            $orginalImageName = str_slug($request->name_az,'-').'.'.$request->file('image')->getClientOriginalExtension();
            $image->save(public_path('images/posts/'.$orginalImageName));
            $tumbnailImageName = rand(). ".".$orginalImageName;
            $image->fit(750, 500, function ($constraint) {
                $constraint->upsize();
            });
            $image->save(public_path('images/posts/'.$tumbnailImageName));
            $post->image = $tumbnailImageName;
        }

        $post->name_az = $request->name_az;
        $post->name_ru = $request->name_ru;
        $post->text_az = $request->text_az;
        $post->text_ru = $request->text_ru;
        $post->keywords = $request->keywords;
        $post->category_id = $request->category_id;
        $post->update();
        $post->tags()->sync($request->tags);
        $request->session()->flash(str_slug('Edit post','-'),'Post edited');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Post $post)
    {
        $post->tags()->detach();
        $post->delete();
        $request->session()->flash(str_slug('Post','-'),'Post Deleted');
        return back();
    }
    /**
     *  Return setting array for table component
     * @return array
     */
    private function getSettingsForTable()
    {
        return  [
            'title' => 'Xəbərlər',
            'table' => 'posts',
            'createButton' => [
                'text' => "Xəbər əlavə et",
                'url' => route('post.create')
            ],
            'columns' => [
                [
                    'label' => 'ID',
                ],
                [
                    'label' => 'Başlıq',
                ],
                [
                    'label' => 'Kateqoriya',
                ],
                [
                    'label' => 'Baxış sayı',
                ],
                [
                    'label' => 'Şəkil',
                ]
            ],
        ];
    }

    /**
     * Return setting array for form component
     * @return array
     */
    private  function  getSettingsForForm()
    {
        return [
            'title' => 'Xəbər əlavə et',
            'flashSessionKey' => 'post',
            'flashSessionValue' => 'Xəbər əlavə edildi',
            'backButton' => [
                'text' => "Əvvələ",
                'url' => route('post.index')
            ]
        ];
    }
}
