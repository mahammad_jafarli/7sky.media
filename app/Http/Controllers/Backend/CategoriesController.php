<?php

namespace App\Http\Controllers\Backend;
use App\Category;
use App\Http\Requests\CategoryRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::orderby('id', 'DESC')->get();
        $settings = $this->getSettingsForTable();
        return view('admin.pages.category.categories', compact('categories', 'settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $setting = $this->getSettingsForForm();
        $mains = Category::where('parent_id', 0)->orderBy('id', 'DESC')->get();
        return view('admin.pages.category.create',compact('setting', 'mains'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request, Category $category)
    {
        $category->create($request->all());
        $request->session()->flash(str_slug('Kateqoriya əlavə et','-'),'Kateqoriya əlavə edildi');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(CategoryRequest $category)
    {
        $mains = Category::where('parent_id', 0)->orderBy('id', 'DESC')->get();
        $setting = $this->getSettingsForForm();
        $setting['title'] = 'Kateqoriyaya düzəliş et';
        return view('admin.pages.category.edit',compact('category','setting', 'mains'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $category->update($request->all());
        $request->session()->flash(str_slug('Kateqoriyaya düzəliş et','-'),'Kateqoriya yeniləndi');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
      public function destroy(Request $request,Category $category)
    {
        $category->delete();
        $request->session()->flash(str_slug('Kateqoriyalar','-'),'Kateqoriya Silindi');
        return back();
    }

    /**
     *  Return setting array for table component
     * @return array
     */
    private function getSettingsForTable()
    {
        return  [
            'title' => 'Категории',
            'table' => 'Kateqoriya',
            'createButton' => [
                'text' => "Kateqoriya əlavə et",
                'url' => route('category.create')
            ],
            'columns' => [
                [
                    'label' => 'ID',
                ],
                [
                    'label' => 'Adı',
                ],
//                [
//                    'label' => 'Əsas Kateqoriya',
//                ]
            ],
        ];
    }

    /**
     * Return setting array for form component
     * @return array
     */
    private  function  getSettingsForForm()
    {
        return [
            'title' => 'Kateqoriya əlavə et',
            'flashSessionKey' => 'kateqoriya',
            'flashSessionValue' => 'Kateqoriya əlavə edildi',
            'backButton' => [
                'text' => "Əvvələ",
                'url' => route('category.index')
            ]
        ];
    }
}
