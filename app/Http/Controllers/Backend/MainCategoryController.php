<?php

namespace App\Http\Controllers\Backend;
use App\Category;
use App\Http\Requests\MainCategoryRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MainCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $mainCategory = Category::where('parent_id', 0)->orderby('id', 'DESC')->get();
        $settings = $this->getSettingsForTable();
        return view('admin.pages.maincats.categories', compact('mainCategory', 'settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $setting = $this->getSettingsForForm();
        return view('admin.pages.maincats.create',compact('setting'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MainCategoryRequest $request, Category $category)
    {
        $request->merge(['parent_id' => 0]);
        $category->create($request->all());
        $request->session()->flash(str_slug('Kateqoriya əlavə et','-'),'Kateqoriya əlavə edildi');
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $mainCategory)
    {
        $setting = $this->getSettingsForForm();
        $setting['title'] = 'Əsas kateqoriyaya düzəliş et';
        return view('admin.pages.maincats.edit',compact('mainCategory','setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MainCategoryRequest $request, Category $mainCategory)
    {
        $request->merge(['parent_id' => 0]);
        $mainCategory->update($request->all());
        $request->session()->flash(str_slug('Əsas kateqoriyaya düzəliş et','-'),'Əsas kateqoriya yeniləndi');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Category $mainCategory)
    {
        $mainCategory->delete();
        $request->session()->flash(str_slug('Əsas kateqoriyalar','-'),'Əsas kateqoriya silindi');
        return back();
    }

    /**
     *  Return setting array for table component
     * @return array
     */
    private function getSettingsForTable()
    {
        return  [
            'title' => 'Основные категории',
            'table' => 'main categories',
            'createButton' => [
                'text' => "Добавлять",
                'url' => route('main_category.create')
            ],
            'columns' => [
                [
                    'label' => 'ID',
                ],
                [
                    'label' => 'Заглавие',
                ]
            ],
        ];
    }

    /**
     * Return setting array for form component
     * @return array
     */
    private  function  getSettingsForForm()
    {
        return [
            'title' => 'Əsas kateqoriya əlavə et',
            'flashSessionKey' => 'maincategory',
            'flashSessionValue' => 'Əsas kateqoriya əlavə edildi',
            'backButton' => [
                'text' => "назад",
                'url' => route('main_category.index')
            ]
        ];
    }
}
