<?php

namespace App\Http\Controllers\Backend;
use App\Http\Requests\TagRequest;
use App\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TagsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::orderBy('id', 'DESC')->get();
        $settings = $this->getSettingsForTable();
        return view('admin.pages.tag.tags', compact('tags', 'settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $setting = $this->getSettingsForForm();
        return view('admin.pages.tag.create',compact('setting'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TagRequest $request, Tag $tag)
    {
        $tag->create($request->all());
        $request->session()->flash(str_slug('Create tag','-'),'Tag created');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        $setting = $this->getSettingsForForm();
        $setting['title'] = 'Edit tag';
        return view('admin.pages.tag.edit',compact('tag','setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TagRequest $request, Tag $tag)
    {
        $tag->update($request->all());
        $request->session()->flash(str_slug('Edit tag','-'),'Tag edited');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Tag $tag)
    {
        $tag->delete();
        $request->session()->flash(str_slug('Tags','-'),'Tag Deleted');
        return back();
    }

    /**
     *  Return setting array for table component
     * @return array
     */
    private function getSettingsForTable()
    {
        return  [
            'title' => 'Tags',
            'table' => 'Tags',
            'createButton' => [
                'text' => "Tag əlavə et",
                'url' => route('tag.create')
            ],
            'columns' => [
                [
                    'label' => 'ID',
                ],
                [
                    'label' => 'Name',
                ]
            ],
        ];
    }

    private  function  getSettingsForForm()
    {
        return [
            'title' => 'Tag əlavə et',
            'flashSessionKey' => 'tag',
            'flashSessionValue' => 'Tag əlavə edildi',
            'backButton' => [
                'text' => "Əvvələ",
                'url' => route('tag.index')
            ],
        ];
    }
        
}
