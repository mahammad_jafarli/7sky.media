<?php

namespace App\Http\Controllers\Frontend;

use App\Category;
use App\Comment;
use App\Post;
use App\Subscribe;
use App\User;
use MetaTag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    public function index()
    {
        $posts = Post::orderBy('id', 'DESC')->paginate(15);
        $postsSlider = Post::orderBy('id', 'DESC')->take(3)->get();
        return view('frontend.pages.home', compact('posts', 'postsSlider', 'postsTrend'));
    }

    public function show(Post $post){

        MetaTag::set('title', $post->name_az);
        MetaTag::set('description', substr((strip_tags($post->text_az)), 0, 200));
        MetaTag::set('image', asset('/images/posts/'.$post->image));
        $post_count = $post->count +=1;
        $post->update([
            "count" => $post_count
        ]);
        $comments = Comment::where('post_id', $post->id)->orderBy('id', 'DESC')->get();
        return view('frontend.pages.post', compact('post', 'comments'));
    }

    public function commentPost(Request $request, Comment $comment){
        if(Auth::guard('web')->check()){
            $comment->user_id = auth()->user()->id;
            $comment->name = auth()->user()->name;
            $comment->email = auth()->user()->email;
            $user = User::find(auth()->user()->id);
            $userBalance = $user->balance +=1;
            $user->update([
               "balance" => $userBalance
            ]);
        }else{
            $comment->name = $request->name;
            $comment->email = $request->email;
            $comment->user_id = null;
        }
        $comment->comment = $request->comment;
        $comment->post_id = $request->post_id;
        $comment->save();
        return back();
    }

    public function subscribe(Request $request)
    {
        $subscribe = new Subscribe();
        $subscribe->email = $request->email;
        $subscribe->save();
        return back();
    }

    public function search()
    {
        $search = $_GET['search'];
        MetaTag::set('title', $search);
        $results = Post::where(
            'name_az', 'Like', '%' . $search . '%'
        )->orWhere(
            'name_ru', 'Like', '%' . $search . '%'
        )->orWhere(
            'text_az', 'Like', '%' . $search . '%'
        )->orWhere(
            'text_ru', 'Like', '%' . $search . '%'
        )->get();
        return view('frontend.pages.results', compact('results'));
    }

    public function category($id)
    {
        $category = Category::findOrFail($id);
        MetaTag::set('title', $category->name_az);
        return view('frontend.pages.category', compact('category'));
    }

}
