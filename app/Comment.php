<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function post(){
        return $this->belongsTo('App\Post');
    }

    protected $fillable = [
        'name','email','comment', 'user_id', 'post_id'
    ];
}
