<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{

    public function blog(){
        return $this->belongsToMany('App\Post', 'post_tags');
    }

    public static function tags()
    {
        return static::orderBy('id', 'DESC')->take(10)->get();
    }

    protected $fillable = [
        'name_ru', 'name_az'
    ];
}
