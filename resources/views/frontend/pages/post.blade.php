@extends('layouts.index')
@php
    $name = 'name_'.app()->getLocale();
    $text = 'text_'.app()->getLocale();
@endphp
@push('keywords')
    {{ $post->keywords }}
@endpush
@section('content')

        <article class="section_margin">
            <figure class="alith_news_img animate-box"><a href="#"><img class="img-responsive" style="width: 100%; object-fit: cover" src="{{ asset('/images/posts/'.$post->image) }}" alt=""/></a></figure>
            <div class="post-content">
                <div class="single-header">
                    <h3 class="alith_post_title">{{ $post->$name }}</h3>
                    <div class="post_meta">
                        <span class="meta_categories"><a href="http://demos.alithemes.com/html/hewo/archive.html">{{ $post->category->$name }}</a></span>
                        <span class="meta_date">{{ date('j M Y ', strtotime($post->created_at)) }}</span>
                    </div>
                </div>
                <div class="single-content animate-box">
                    <div class="dropcap column-1 animate-box">
                         {!! $post->$text !!}
                    </div>
                    <div class="post-tags">
                        <div class="post-tags-inner">
                            @foreach($post->tags as $tag)
                            <a rel="tag" href="#">#{{ $tag->$name }}</a>
                            @endforeach
                        </div>
                    </div>
                    <div class="post-share">
                        @php
                            $linkPost = str_slug($post->name_az, '-');
                        @endphp
                        <ul>
                            <li class="facebook"><a href="https://www.facebook.com/sharer/sharer.php?u={{ url($post->id.'-'.$linkPost) }}"><i class="fab fa-facebook"></i></a></li>
                            <li class="twitter"><a href="https://twitter.com/home?status={{ url($post->id.'-'.$linkPost) }}"><i class="fab fa-twitter"></i></a></li>
                            <li class="google-plus"><a href="https://plus.google.com/share?url={{ url($post->id.'-'.$linkPost) }}"><i class="fab fa-google-plus"></i></a></li>
                        </ul>
                    </div>

                </div> <!--single content-->
                <div class="single-comment">
                    <section id="comments">
                        <h4 class="single-comment-title">{{ Lang::get('content.comments') }}</h4>
                        <div class="comments-inner clr">
                            <div class="comments-title"> <p>{{ $comments->count() }} {{ Lang::get('content.comment') }}</p></div>
                            <ol class="commentlist">
                                @foreach($comments as $comment)
                                <li id="li-comment-4">
                                    <article class="comment even thread-even depth-1 clr" id="comment-4">
                                        <div class="comment-author vcard"> <img width="60" height="60" src="{{ asset('/images/user.png') }}" alt=""></div>
                                        <div class="comment-details clr ">
                                            <header class="comment-meta"> <strong class="fn"> {{ $comment->name }} </strong> <span class="comment-date">{{ date('j M Y ', strtotime($comment->created_at)) }} </span></header>
                                            <div class="comment-content entry clr">
                                                <p>{{ $comment->comment }}</p>
                                            </div>
                                            {{--<div class="reply comment-reply-link-div"> <a aria-label="Reply to spadmin" href="single.html#respond" class="comment-reply-link" rel="nofollow">Reply</a></div>--}}
                                        </div>
                                    </article>
                                </li>
                                @endforeach

                            </ol> <!--comment list-->
                            <nav role="navigation" class="comment-navigation clr">
                                {{--<div class="nav-previous span_1_of_2 col col-1"></div>--}}
                                {{--<div class="nav-next span_1_of_2 col"> <a href="single.html#comments">Newer Comments →</a></div>--}}
                            </nav> <!--comment nav-->
                            <div class="comment-respond" id="respond">
                                <h3 class="comment-reply-title" id="reply-title">{{ Lang::get('content.commentWrite') }}<small><a href="single.html#respond" id="cancel-comment-reply-link" rel="nofollow"><i class="fa fa-times"></i></a></small></h3>
                                <form class="comment-form" id="commentform" method="post" action="{{ url('commentPost') }}">
                                    @csrf
                                    <input type="hidden" value="{{ $post->id }}" name="post_id">
                                    <p class="comment-notes"><span id="email-notes">{{ Lang::get('content.commentDesc') }}</span></p>
                                    @guest
                                    <div class="row">
                                        <div class="comment-form-author col-sm-12 col-md-6">
                                            <label for="author">{{ Lang::get('content.name') }} *</label>
                                            <input type="text" size="30" placeholder="{{ Lang::get('content.name') }} *" name="name" value="{{ old('name') }}" id="author" required>
                                        </div>
                                        <div class="comment-form-email col-sm-12 col-md-6">
                                            <label for="email">{{ Lang::get('content.mail') }} *</label>
                                            <input type="email" size="30" placeholder="{{ Lang::get('content.mail') }}" name="email" id="email" value="{{ old('email') }}" required>
                                        </div>
                                    </div>
                                    @endguest
                                    <p class="comment-form-comment"><label for="comment">{{ Lang::get('content.comment') }}</label><textarea aria-required="true" placeholder="{{ Lang::get('content.comment') }}" rows="8" cols="45" name="comment" id="comment">{{ old('comment') }}</textarea></p>

                                    <p class="form-submit"><input type="submit" value="{{ Lang::get('content.send') }}" class="submit" id="submit" name="submit"> <input type="hidden" id="comment_post_ID" value="80" name="comment_post_ID"> <input type="hidden" value="0" id="comment_parent" name="comment_parent"></p>
                                </form>
                            </div> <!--comment form-->

                        </div>
                    </section>
                </div>
            </div>
        </article>
        <div class="single-more-articles single-disable-inview">
            <h4><span>{{ Lang::get('content.similar') }}</span></h4>
            <span class="single-more-articles-close-button"><i class="fa fa-times" aria-hidden="true"></i></span>
            <div class="latest_style_2">
                @foreach($post->category->post->take(3) as $post)
                    @php
                        $linkPost = str_slug($post->name_az, '-');
                    @endphp
                    <div class="latest_style_2_item">
                        <figure class="alith_news_img"><a href="{{ url($post->id.'-'.$linkPost) }}"><img class="hover_grey" src="{{ asset('/images/posts/'.$post->image) }}" alt="{{ $post->$name }}"></a></figure>
                        <h3 class="alith_post_title"><a href="{{ url($post->id.'-'.$linkPost) }}">{!!  substr(($post->$name), 0,60) !!}</a></h3>
                    </div>
                @endforeach
            </div>
        </div> <!--end single more articles-->

@endsection