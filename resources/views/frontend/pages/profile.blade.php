@extends('layouts.index')
@php
    $name = 'name_'.app()->getLocale();
    $text = 'text_'.app()->getLocale();
@endphp
@section('content')

    <div class="archive-header">
        <div class="post-author-info">
            <img class="section_margin_20 img-responsive" src="{{ asset('/images/user.png') }}" alt="" style="width: 177px;"/>
            <div class="archive-title"><h2>{{ auth()->user()->name }}</h2></div>
            <p>{{ auth()->user()->email }}</p>

            <button class="btn balance">{{ auth()->user()->balance }}</button>
        </div>
    </div>

@endsection