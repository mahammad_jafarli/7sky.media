@extends('layouts.index')
@php
    $name = 'name_'.app()->getLocale();
    $text = 'text_'.app()->getLocale();
@endphp
@section('content')
    <div class="owl-carousel owl-theme js section_margin line_hoz animate-box" id="slideshow_face">
        @foreach($postsSlider as $post)
            @php
                $linkPost = str_slug($post->name_az, '-');
            @endphp
        <div class="item">
            <figure class="alith_post_thumb_big">
                <span class="post_meta_categories_label">{{ $post->category->$name }}</span>
                <a href="{{ url($post->id.'-'.$linkPost) }}">
                    <img src="{{ asset('/images/posts/'.$post->image) }}" class="img-responsive" style="width: 100%; object-fit: cover" alt="{{ $post->name_az }}"/>
                </a>
            </figure>
            <h3 class="alith_post_title animate-box" data-animate-effect="fadeInUp">
                <a href="{{ url($post->id.'-'.$linkPost) }}">{{ $post->$name }}</a>
            </h3>
            <div class="alith_post_content_big">
                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        <div class="post_meta_center animate-box">
                            <span class="post_meta_date">{{ date('j M Y ', strtotime($post->created_at)) }}</span>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12 animate-box">
                        <p class="alith_post_except">
                            {!! substr((strip_tags($post->$text)), 0, 200) !!}
                        </p>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <div class="post_list post_list_style_1">
        <div class="alith_heading">
            <h2 class="alith_heading_patern_2">{{ Lang::get('nav.latest') }}</h2>
        </div>
        @foreach($posts as $post)
            @php
                $linkPost = str_slug($post->name_az, '-');
            @endphp
        <article class="row section_margin animate-box">
            <div class="col-md-3 animate-box">
                <figure class="alith_news_img"><a href="{{ url($post->id.'-'.$linkPost) }}"><img src="{{ asset('/images/posts/'.$post->image) }}" alt=""/></a></figure>
            </div>
            <div class="col-md-9 animate-box">
                <h3 class="alith_post_title"><a href="{{ url($post->id.'-'.$linkPost) }}">{{ $post->$name }}</a></h3>
                <div class="post_meta">
                    <span class="meta_categories"><a href="http://demos.alithemes.com/html/hewo/archive.html">{{ $post->category->$name }}</a>
                    <span class="meta_date">{{ date('j M Y ', strtotime($post->created_at)) }}</span>
                </div>
            </div>
        </article>
        @endforeach


        <div class="site-pagination animate-box">
            <ul class="page-numbers">
                {{--<li><a href="index.html#" class="prev page-numbers">PREV</a></li>--}}
                {{--<li><span class="page-numbers current" aria-current="page">1.</span></li>--}}
                {{--<li><a href="index.html#" class="page-numbers">2.</a></li>--}}
                {{--<li><a href="index.html#" class="page-numbers">4.</a></li>--}}
                {{--<li><a href="index.html#" class="next page-numbers">NEXT</a></li>--}}
                {!! $posts->links('vendor.pagination.default'); !!}
            </ul>
        </div>
    </div>
@endsection
