@extends('layouts.index')
@php
    $name = 'name_'.app()->getLocale();
    $text = 'text_'.app()->getLocale();
@endphp
@section('breadcrumb')
    <div class="container-fluid">
        <div class="container animate-box">
            <div class="row">
                <div class="archive-header">
                    <div class="archive-title"><h2>{{ $category->$name }}</h2></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="post_list post_list_style_1">
        @foreach($category->post as $post)
            @php
                $linkPost = str_slug($post->name_az, '-');
            @endphp
            <article class="row section_margin animate-box">
                <div class="col-md-3 animate-box">
                    <figure class="alith_news_img"><a href="{{ url($post->id.'-'.$linkPost) }}"><img src="{{ asset('/images/posts/'.$post->image) }}" alt=""/></a></figure>
                </div>
                <div class="col-md-9 animate-box">
                    <h3 class="alith_post_title"><a href="{{ url($post->id.'-'.$linkPost) }}">{{ $post->$name }}</a></h3>
                    <div class="post_meta">
                    <span class="meta_categories"><a href="http://demos.alithemes.com/html/hewo/archive.html">{{ $post->category->$name }}</a>
                    <span class="meta_date">{{ date('j M Y ', strtotime($post->created_at)) }}</span>
                    </div>
                </div>
            </article>
        @endforeach

        {{--<div class="site-pagination animate-box">--}}
            {{--<ul class="page-numbers">--}}
                {{--<li><a href="index.html#" class="prev page-numbers">PREV</a></li>--}}
                {{--<li><span class="page-numbers current" aria-current="page">1.</span></li>--}}
                {{--<li><a href="index.html#" class="page-numbers">2.</a></li>--}}
                {{--<li><a href="index.html#" class="page-numbers">4.</a></li>--}}
                {{--<li><a href="index.html#" class="next page-numbers">NEXT</a></li>--}}
                {{--{!! $posts->links('vendor.pagination.default'); !!}--}}
            {{--</ul>--}}
        {{--</div>--}}
    </div>
@endsection
