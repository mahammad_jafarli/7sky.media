@extends('layouts.index')
@php
    $name = 'name_'.app()->getLocale();
    $text = 'text_'.app()->getLocale();
@endphp
@section('content')
    <div class="post_list post_list_style_1">
        <div class="alith_heading">
            <h2 class="alith_heading_patern_2">Results</h2>
        </div>
        @foreach($results as $post)
            @php
                $linkPost = str_slug($post->name_az, '-');
            @endphp
            <article class="row section_margin animate-box">
                <div class="col-md-3 animate-box">
                    <figure class="alith_news_img"><a href="{{ url($post->id.'-'.$linkPost) }}"><img src="{{ asset('/images/posts/'.$post->image) }}" alt=""/></a></figure>
                </div>
                <div class="col-md-9 animate-box">
                    <h3 class="alith_post_title"><a href="{{ url($post->id.'-'.$linkPost) }}">{{ $post->$name }}</a></h3>
                    <div class="post_meta">
                        <span class="meta_categories"><a href="http://demos.alithemes.com/html/hewo/archive.html">{{ $post->category->$name }}</a>
                        <span class="meta_date">{{ date('j M Y ', strtotime($post->created_at)) }}</span>
                    </div>
                </div>
            </article>
        @endforeach
    </div>
@endsection
