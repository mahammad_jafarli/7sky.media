<div id="sidebar-wrapper">
    <div class="sidebar-inner">
        <div class="off-canvas-close"><span>CLOSE</span></div>
        <div class="sidebar-widget">
            <div class="widget-title-cover">
                <h4 class="widget-title"><span>7 SKY</span></h4>
            </div>
            <ul class="menu" id="sidebar-menu">

                <li class="menu-item"><a href="{{ url('/contact') }}">Contact</a></li>
            </ul>
        </div>

        <div class="sidebar-widget">
            <div class="widget-title-cover"><h4 class="widget-title"><span>Trending</span></h4></div>
            <div class="latest_style_2">
                @foreach($postsTrend as $post)
                    @php
                        $linkPost = str_slug($post->name_az, '-');
                    @endphp
                <div class="latest_style_2_item">
                    <figure class="alith_news_img"><a href="{{ url($post->id.'-'.$linkPost) }}"><img src="{{ asset('/images/posts/'.$post->image) }}" class="img-responsive" style="width: 100%; object-fit: cover" alt="{{ $post->name_az }}"/></a></figure>
                    <h3 class="alith_post_title"><a href="{{ url($post->id.'-'.$linkPost) }}">{{ $post->$name }}</a></h3>

                    <div class="post_meta">
                        <p class="meta"><span>{{ date('j M Y ', strtotime($post->created_at)) }}</span> <span>{{ $post->count }} views</span></p>
                    </div>
                </div>
                @endforeach
            </div>
        </div> <!--.sidebar-widget-->

    </div>
</div>
<!--#sidebar-wrapper-->