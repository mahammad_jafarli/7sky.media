<div class="container-fluid">
    <div class="container animate-box">
        <div class="bottom margin-15">
            <div class="row">
                {{--<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">--}}
                    {{--<div class="sidebar-widget">--}}
                        {{--<div class="widget-title-cover"><h4 class="widget-title"><span>Most comments</span></h4></div>--}}
                        {{--<div class="latest_style_3">--}}
                            {{--<div class="latest_style_3_item">--}}
                                {{--<span class="item-count vertical-align">1.</span>--}}
                                {{--<div class="alith_post_title_small">--}}
                                    {{--<a href="single.html"><strong>Frtuitous spluttered unlike ouch vivid blinked far inside</strong></a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="latest_style_3_item">--}}
                                {{--<span class="item-count vertical-align">2.</span>--}}
                                {{--<div class="alith_post_title_small">--}}
                                    {{--<a href="single.html"><strong>Against and lantern where a and gnashed nefarious</strong></a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="latest_style_3_item">--}}
                                {{--<span class="item-count vertical-align">3.</span>--}}
                                {{--<div class="alith_post_title_small">--}}
                                    {{--<a href="single.html"><strong>Ouch oh alas crud unnecessary invaluable some</strong></a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="latest_style_3_item">--}}
                                {{--<span class="item-count vertical-align">4.</span>--}}
                                {{--<div class="alith_post_title_small">--}}
                                    {{--<a href="single.html"><strong>And far hey much hello and bashful one save less</strong></a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="sidebar-widget">
                        <div class="widget-title-cover"><h4 class="widget-title"><span>{{ Lang::get('nav.latest') }}</span></h4></div>
                        <div class="latest_style_2">
                            @foreach($postsSlider as $post)
                                @php
                                    $linkPost = str_slug($post->name_az, '-');
                                @endphp
                                <div class="latest_style_2_item">
                                    <figure class="alith_news_img"><a href="{{ url($post->id.'-'.$linkPost) }}"><img alt="{{ $post->name_az }}" src="{{ asset('/images/posts/'.$post->image) }}" class="hover_grey img-responsive" style="width: 100%; object-fit: cover"></a></figure>
                                    <h3 class="alith_post_title"><a href="{{ url($post->id.'-'.$linkPost) }}">{{ substr(($post->$name), 0,80)}}</a></h3>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                {{--<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">--}}
                    {{--<div class="sidebar-widget">--}}
                        {{--<div class="widget-title-cover"><h4 class="widget-title"><span>Categories</span></h4></div>--}}
                        {{--<ul class="bottom_menu">--}}
                            {{----}}
                            {{--<li><a href="index.html#" class=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp; Business</a></li>--}}
                            {{----}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">--}}
                    {{--<div class="sidebar-widget">--}}
                        {{--<div class="widget-title-cover"><h4 class="widget-title"><span>Instagram</span></h4></div>--}}
                        {{--<ul class="alith-instagram-grid-widget alith-clr alith-row alith-gap-10">--}}
                            {{--<li class="wow fadeInUp alith-col-nr alith-clr alith-col-3 animated">--}}
                                {{--<a class="" target="_blank" href="index.html#">--}}
                                    {{--<img class="" title="" alt="" src="assets/images/thumb-square-1.png">--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li class="wow fadeInUp alith-col-nr alith-clr alith-col-3 animated">--}}
                                {{--<a class="" target="_blank" href="index.html#">--}}
                                    {{--<img class="" title="" alt="" src="assets/images/thumb-square-2.png">--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li class="wow fadeInUp alith-col-nr alith-clr alith-col-3 animated">--}}
                                {{--<a class="" target="_blank" href="index.html#">--}}
                                    {{--<img class="" title="" alt="" src="assets/images/thumb-square-3.png">--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li class="wow fadeInUp alith-col-nr alith-clr alith-col-3 animated">--}}
                                {{--<a class="" target="_blank" href="index.html#">--}}
                                    {{--<img class="" title="" alt="" src="assets/images/thumb-square-4.png">--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li class="wow fadeInUp alith-col-nr alith-clr alith-col-3 animated">--}}
                                {{--<a class="" target="_blank" href="index.html#">--}}
                                    {{--<img class="" title="" alt="" src="assets/images/thumb-square-5.png">--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li class="wow fadeInUp alith-col-nr alith-clr alith-col-3 animated">--}}
                                {{--<a class="" target="_blank" href="index.html#">--}}
                                    {{--<img class="" title="" alt="" src="assets/images/thumb-square-1.png">--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li class="wow fadeInUp alith-col-nr alith-clr alith-col-3 animated">--}}
                                {{--<a class="" target="_blank" href="index.html#">--}}
                                    {{--<img class="" title="" alt="" src="assets/images/thumb-square-2.png">--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li class="wow fadeInUp alith-col-nr alith-clr alith-col-3 animated">--}}
                                {{--<a class="" target="_blank" href="index.html#">--}}
                                    {{--<img class="" title="" alt="" src="assets/images/thumb-square-3.png">--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li class="wow fadeInUp alith-col-nr alith-clr alith-col-3 animated">--}}
                                {{--<a class="" target="_blank" href="index.html#">--}}
                                    {{--<img class="" title="" alt="" src="assets/images/thumb-square-4.png">--}}
                                {{--</a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div> <!--.row-->
        </div>
    </div>
</div>
<div class="container-fluid alith_footer_right_reserved">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-12 bottom-logo">
                <h1 class="logo"><a href="{{ url('/') }}">7 Sky</a></h1>
                <div class="tagline social">
                    <ul>
                        @foreach($socials as $social)
                        <li class="facebook"><a href="{{ $social->link }}" target="_blank"><i class="fab fa-{{ $social->icon }}"></i></a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-12 col-md-12 coppyright"> <p>© Copyright 2019, All rights reserved. Site by <a href="http://jafarli.me" title="Jafarli.me"><b>Jafarli.me</b></a></p> </div>
        </div>
    </div>
</div>