@php
    $name = 'name_'.app()->getLocale();
@endphp

<ul id="main-menu">

    {{--@foreach($categories as $category)--}}
        {{--@if($category->parent_id == 0)--}}
            {{--<li><a href="#">{{ $category->$name }}</a></li>--}}
        {{--@endif--}}
    {{--@endforeach--}}
        @foreach($categories as $category)
            @php
                $linkCategory = str_slug($category->name_az, '-')
            @endphp
            @if($category->parent_id == 0)
                <li class="menu-item-has-children"><a href="{{ url('/category/'.$category->id.'-'.$linkCategory) }}">{{ $category->$name }}</a>
                    <ul class="sub-menu">
                        @foreach($category->parent_category as $parent)
                            @php
                                $linkParent = str_slug($parent->name_az, '-')
                            @endphp
                            <li><a href="{{ url('/category/'.$parent->id.'-'.$linkParent) }}">{{ $parent->$name }}</a></li>
                        @endforeach
                    </ul>
                </li>
            @endif
        @endforeach
</ul>