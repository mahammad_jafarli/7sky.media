<!-- Meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>{{ MetaTag::get('title') }}</title>

{!! MetaTag::tag('description') !!}
{!! MetaTag::tag('image') !!}

{!! MetaTag::openGraph() !!}

{!! MetaTag::twitterCard() !!}

{{--Set default share picture after custom section pictures--}}
{!! MetaTag::tag('image', asset('images/default-logo.png')) !!}

<meta name="keywords" content="7 sky, @stack('keywords'),">


<!-- Bootstrap, Font Awesome, Aminate, Owl Carausel, Normalize CSS -->
<link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css"/>
{{--<link href="{{ url('http://demos.alithemes.com/html/hewo/assets/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>--}}
<link rel="stylesheet" href="{{ url('https://use.fontawesome.com/releases/v5.3.1/css/all.css') }}" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<link href="{{ asset('assets/css/owl.carousel.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/css/owl.theme.default.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/css/animate.css') }}" rel="stylesheet" type="text/css"/>
@stack('stylesheets')
<link href="{{ asset('assets/css/normalize.css') }}?ver=1.0.0" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/css/slicknav.min.css') }}" rel="stylesheet" type="text/css"/>

<!-- Site CSS -->

<link href="{{ asset('assets/css/main.css') }}?ver=1.0.2" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet" type="text/css"/>

<!-- Modernizr JS -->
<script src="{{ asset('assets/js/modernizr-3.5.0.min.js') }}"></script>
<!-- Google fonts -->
<link href="{{ url('https://fonts.googleapis.com/css?family=IBM+Plex+Serif:300,400,500') }}" rel="stylesheet">
<link href="{{ url('https://fonts.googleapis.com/css?family=Playfair+Display') }}" rel="stylesheet">