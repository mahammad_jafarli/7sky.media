<div class="top_bar margin-15">
    <div class="row">
        <div class="col-md-6 col-sm-12 time">
            <div class="off-canvas-toggle" id="off-canvas-toggle"><span></span><p class="sidebar-open">{{ Lang::get('nav.more') }}</p></div>
            <i class="far fa-clock-o"></i><span>&nbsp;&nbsp;&nbsp;<a href="{{ url('lang/az') }}">AZE</a> | <a href="{{ url('lang/ru') }}">RUS</a></span>&nbsp;&nbsp;&nbsp;&nbsp;
            @guest
                <i class="far fa-user"></i><span>&nbsp<a href="{{ url('/login') }}">{{ Lang::get('nav.login') }}</a></span>&nbsp;&nbsp;&nbsp;&nbsp;
                <i class="fas fa-sign-out-alt"></i><span>&nbsp<a href="{{ url('/register') }}">{{ Lang::get('nav.signin') }}</a></span>
                @else
                <i class="far fa-user"></i><span>&nbsp<a href="{{ url('/user') }}">{{ Lang::get('nav.user') }}</a></span>&nbsp;&nbsp;&nbsp;&nbsp;
                <i class="fas fa-sign-out-alt"></i>
                <span>&nbsp;&nbsp;&nbsp;
                    <a  href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        {{ Lang::get('nav.logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                </span>
            @endif
        </div>
        <div class="col-md-6 col-sm-12 social">
            <ul>
                @foreach($socials as $social)
                    <li><a href="{{ $social->link }}" target="_blank"><i class="fab fa-{{ $social->icon }}"></i></a></li>
                @endforeach
            </ul>
            <div class="top-search">
                <i class="fa fa-search"></i><span>{{ Lang::get('nav.search') }}</span>
            </div>
            <div class="top-search-form">
                <form action="{{ url('/search') }}" class="search-form" method="get" role="search">
                    <label>
                        <span class="screen-reader-text">Search for:</span>
                        <input type="search" name="search" value="" placeholder="{{ Lang::get('nav.search') }} …" class="search-field">
                    </label>
                    <input type="submit" value="{{ Lang::get('nav.search') }}" class="search-submit">
                </form>
            </div>
        </div>
    </div>
</div>