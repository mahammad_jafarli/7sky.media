<aside class="col-md-4 sidebar_right">
    <div class="sidebar-widget animate-box">
        <div class="widget-title-cover"><h4 class="widget-title"><span>{{ Lang::get('nav.trending') }}</span></h4></div>
        <div class="latest_style_1">
            @php
                $a = 1;
            @endphp
            @foreach($postsTrend as $post)
                @php
                    $linkPost = str_slug($post->name_az, '-');
                    $i = $a++;
                @endphp
                <div class="latest_style_1_item">
                    <span class="item-count vertical-align">{{ $i }}.</span>
                        <div class="alith_post_title_small">
                            <a href="{{ url($post->id.'-'.$linkPost) }}"><strong>{!! substr(($post->$name), 0,70) !!}</strong></a>
                            <p class="meta"><span>{{ date('j M Y ', strtotime($post->created_at)) }}</span> <span>{{ $post->count }} {{ Lang::get('content.view') }}</span></p>
                        </div>
                    <figure class="alith_news_img"><a href="{{ url($post->id.'-'.$linkPost) }}"><img src="{{ asset('/images/posts/'.$post->image) }}" class="img-responsive" style="width: 100%; object-fit: cover" alt="{{ $post->name_az }}"/></a></figure>
                </div>
            @endforeach
        </div>
    </div> <!--.sidebar-widget-->
    <div class="sidebar-widget animate-box">
        <div class="widget-title-cover"><h4 class="widget-title"><span>{{ Lang::get('content.subscribe') }}</span></h4></div>
        <form action="{{ url('postSubscribe') }}" class="search-form" method="POST">
            @csrf
            <label>
                <input type="text" name="email" value="{{ old('email') }}" placeholder="{{ Lang::get('content.mail') }}" class="search-field">
            </label>
            <input type="submit" value="{{ Lang::get('content.subscribeBtn') }}" class="search-submit">
        </form>
    </div> <!--.sidebar-widget-->
    <div class="sidebar-widget animate-box">
        <div class="widget-title-cover"><h4 class="widget-title"><span>{{ Lang::get('content.tags') }}</span></h4></div>
        <div class="alith_tags_all">
            @foreach($tags as $tag)
            <a href="#" class="alith_tagg">#{{ $tag->$name }}</a>
           @endforeach
        </div>
    </div> <!--.sidebar-widget-->

</aside>