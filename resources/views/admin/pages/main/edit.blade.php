@component('admin.components.form',$setting)
    {{-- Component content--}}
    <form id="form_validation" action="{{route('main.update',['main' => $main->id])}}" method="post" enctype="multipart/form-data">
        @csrf
        {{method_field('PUT')}}
        <div class="row clearfix">
            <div class="col-sm-12">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input name="photo1" type="file"  class="form-control  post-image-logo" id="logo">
                        {{--<label for="logo" style="cursor: pointer">--}}
                            {{--<img class="img-responsive thumbnail post-img-preview-logo" src="{{asset('images/mains/'.$main->logo)}}">--}}
                        {{--</label>--}}
                    </div>
                    <label class="form-label">Logo</label>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input name="photo2" type="file"  class="form-control   post-image-share" id="site_share_img">
                        {{--<label for="site_share_img" style="cursor: pointer">--}}
                            {{--<img class="img-responsive thumbnail post-img-preview-share" src="{{asset('images/mains/'.$main->site_share_img)}}">--}}
                        {{--</label>--}}
                    </div>
                    <label class="form-label">Site Share Image </label>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input name="photo3" type="file"  class="form-control  post-image-single" id="singe_page_img">
                        {{--<label for="singe_page_img" style="cursor: pointer">--}}
                            {{--<img class="img-responsive thumbnail post-img-preview-single" src="{{asset('mains/'.$main->singe_page_img)}}">--}}
                        {{--</label>--}}
                    </div>
                    <label class="form-label">Single Page Image</label>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input name="site_title" type="text"  class="form-control" value="{{$main->site_title}}">
                        <label class="form-label">Title</label>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-group form-float">

                    <div class="form-line">
                        <textarea name="site_desc" id="tinymce" aria-hidden="true"> {{$main->site_desc}} </textarea>
                    </div>
                    <label class="form-label">Description</label>

                </div>
            </div>
            <div class="col-sm-12">
                <div class="demo-switch">
                    <button type="submit" class="btn btn-success waves-effect right">Edit main</button>
                </div>
            </div>
        </div>
    </form>
@endcomponent