@extends('layouts.backend')
@section('title')
    Magazine
    @endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            {{--@component('admin.components.table',$settings)--}}
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <div class="icon bg-red">
                                <i class="material-icons">face</i>
                            </div>
                            <div class="content">
                                <div class="text">Пользователи</div>
                                <div class="number count-to" data-from="0" data-to="125" data-speed="1000" data-fresh-interval="20">{{ $users->count() }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <div class="icon bg-red">
                                <i class="material-icons">assignment</i>
                            </div>
                            <div class="content">
                                <div class="text">Новости</div>
                                <div class="number count-to" data-from="0" data-to="125" data-speed="1000" data-fresh-interval="20">{{ $posts->count() }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <div class="icon bg-red">
                                <i class="material-icons">comment</i>
                            </div>
                            <div class="content">
                                <div class="text">Комментарии</div>
                                <div class="number count-to" data-from="0" data-to="125" data-speed="1000" data-fresh-interval="20">{{ $comments->count() }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <div class="icon bg-red">
                                <i class="material-icons">bookmark_border</i>
                            </div>
                            <div class="content">
                                <div class="text">Категории</div>
                                <div class="number count-to" data-from="0" data-to="125" data-speed="1000" data-fresh-interval="20">{{ $categories->count() }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                 {{--Component content--}}
                    {{--<tr>--}}
                        {{--<td>--}}
                            {{--<img src="{{asset('images/mains/'.$main->logo)}}" alt="" width="150">--}}
                        {{--</td>--}}
                        {{--<td>--}}
                            {{--<img src="{{asset('images/mains/'.$main->site_share_img)}}" alt="" width="150">--}}
                        {{--</td>--}}
                        {{--<td>--}}
                            {{--<img src="{{asset('images/mains/'.$main->singe_page_img)}}" alt="" width="150">--}}
                        {{--</td>--}}
                        {{--<td>{{$main->site_title}}</td>--}}
                        {{--<td>{!! $main->site_desc !!}</td>--}}
                        {{--<td>--}}
                            {{--<a class="btn bg-blue btn-circle waves-effect waves-circle waves-float" href="{{route('main.edit',['main' => $main->id])}}"><i class="material-icons">edit</i></a>--}}
                        {{--</td>--}}
                    </tr>
            {{--@endcomponent--}}

        </div>
    </div>
@endsection