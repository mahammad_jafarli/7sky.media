@extends('layouts.backend')
@php
    $name = 'name_'.app()->getLocale();
    $text = 'text_'.app()->getLocale();
@endphp
@section('title', $post->name_ru)
@section('content')
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <a href="{{ url('lang/ru') }}" class="btn btn-success">Rus</a>
                                
                                <a href="{{ url('lang/az') }}" class="btn btn-primary">Aze</a>
                            </h2>
                        </div>
                        <div class="body">
                            <h4>{{ $post->$name }}</h4>
                            <span>{{ $post->category->$name }}</span>
                            <a class="btn bg-blue waves-effect waves-float right" href="{{route('post.edit',['post' => $post->id])}}"><i class="material-icons">edit</i></a>
                            <hr>
                            <p class="m-t-15 m-b-30">
                                {!! $post->$text !!}
                                <hr>
                                @foreach($post->tags as $tag)
                                    <span class="defult">#{{ $tag->$name }}</span>
                                @endforeach
                                <hr>
                            </p>
                            <img src="{{ asset('/images/posts/'.$post->image) }}" class="img-responsive" alt="">
                            <hr>
                            <div class="body table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>NAME</th>
                                        <th>Comment</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($post->comment as $comment)
                                    <tr>
                                        <th scope="row">{{ $comment->id }}</th>
                                        <td>{{ $comment->name }}</td>
                                        <td>{{ $comment->comment }}</td>
                                        <td> 
                                            <form action="{{route('comment.destroy',['comment' => $comment->id])}}" method="post">
                                                {{ method_field('delete') }}
                                                @csrf
                                                <button type="submit" class="btn bg-red btn-circle waves-effect waves-circle waves-float">
                                                    <i class="material-icons">delete</i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        </div>
                    </div>
                </div>
@endsection