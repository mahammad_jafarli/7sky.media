@component('admin.components.table',$settings)
    {{-- Component content--}}
    @foreach($posts as $post)
        <tr>
            <td>{{$post->id}}</td>
            <td>{{$post->name_az}}</td>
            <td>{{$post->category->name_az}}</td>
            <td>{{$post->count}}</td>
            <td>
                <img src="{{asset('images/posts/'.$post->image)}}" alt="" width="150">
            </td>
            <td>
                <a class="btn btn-info btn-circle waves-effect waves-circle waves-float" href="{{ route('post.show', ['post' => $post->id]) }}"><i class="material-icons">remove_red_eye</i></a>
                <a class="btn bg-blue btn-circle waves-effect waves-circle waves-float" href="{{route('post.edit',['post' => $post->id])}}"><i class="material-icons">edit</i></a>
                <form action="{{route('post.destroy',['post' => $post->id])}}" method="post">
                    {{ method_field('delete') }}
                    @csrf
                    <button type="submit" class="btn bg-red btn-circle waves-effect waves-circle waves-float">
                        <i class="material-icons">delete</i>
                    </button>
                </form>
            </td>
        </tr>
    @endforeach
@endcomponent