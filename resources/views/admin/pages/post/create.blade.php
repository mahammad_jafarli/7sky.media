@component('admin.components.form',$settings)
    {{-- Component content--}}
    <form id="form_validation" action="{{route('post.store')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row clearfix">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                @if(!empty($errors->first()))
                    <div class="alert alert-danger">
                        <strong>Diqqət!</strong> {{ $errors->first() }}.
                    </div>  
                @endif      
                <div class="card">
                    
                    <br>        
                    <div class="header">
                        <h2>
                            Adı
                        </h2>
                    </div>
                    <div class="body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs tab-nav-right" role="tablist">
                            <li role="presentation" class="active"><a href="#rusname" data-toggle="tab">Russian</a></li>
                            <li role="presentation"><a href="#spnname" data-toggle="tab">Azerbajan</a></li>
                        </ul>
                        <br>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="rusname">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="name_ru" id="title" class="form-control" value="{{ old('name_ru') }}" >
                                        <label class="form-label">Title Rus</label>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="spnname">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="name_az" id="title" class="form-control" value="{{ old('name_az') }}" >
                                        <label class="form-label">Title Aze</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Mətn
                        </h2>
                    </div>
                    <div class="body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs tab-nav-right" role="tablist">
                            <li role="presentation" class="active"><a href="#rus" data-toggle="tab">Russian</a></li>
                            <li role="presentation"><a href="#spn" data-toggle="tab">Azerbaijan</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="rus">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <textarea name="text_ru" class="textru my-editor" id="tinymce" cols="30" rows="10" >{{ old('text_ru') }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="spn">
                                <div class="form-group form-float">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <textarea name="text_az" class="textsp my-editor" id="tinymce" cols="30" rows="10" >{{ old('text_az') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Keywords ( Açar sözlər )
                        </h2>
                    </div>
                    <div class="body">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" name="keywords" class="form-control" value="{{ old('keywords') }}">
                                <label class="form-label">Keywords</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="card">
                        <div class="header">
                            <h2>Kateqoriya</h2>
                        </div>
                        <div class="body">
                            <div class="input-group">
                                <div class="form-line">
                                    <select name="category_id" id="cat" class="bootstrap-select show-tick" data-live-search="true" >
                                        <option value="">Secin</option>
                                        @foreach($categories as $category)
                                            @if($category->parent_id == 0)
                                                <option value="{{$category->id}}">{{$category->name_az}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    <div class="sub" style="display: none;">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="card">
                        <div class="header">
                            <h2>Taq</h2>
                        </div>
                        <div class="body">
                            <div class="input-group">
                                <div class="form-line">
                                    <select name="tags[]" class="show-tick" data-live-search="true" multiple data-selected-text-format="count">
                                        @foreach($tags as $tag)
                                            <option value="{{$tag->id}}">{{$tag->name_az}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input name="image" type="file" class="form-control hidden post-image" id="postImage" required>
                        <label for="postImage" style="cursor: pointer">
                            <img class="img-responsive thumbnail post-img-preview" src="{{asset('admin/images/image-gallery/thumb/thumb-15.jpg')}}">
                        </label>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="demo-switch">
                    <button type="submit" class="btn btn-success waves-effect right">Əlavə et</button>
                </div>
            </div>
        </div>
    </form>
@endcomponent