@component('admin.components.form', $settings)
    {{-- @foreach($post->images as $image)
        <div class="col-sm-6">
            <div class="form-group form-float">
                <div class="col-sm-6" style="height:90px">
                    <img class="img-responsive thumbnail post-img-preview" src="{{asset('images/posts/'.$image->image)}}">
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">

                    <form action="{{route('image.destroy',['image' => $image->id])}}" method="post">
                        {{ method_field('delete') }}
                        @csrf
                        <button type="submit" class="btn bg-red btn-circle waves-effect waves-circle waves-float">
                            <i class="material-icons">delete</i>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    @endforeach --}}
    <form action="{{ route('post.update',['post' => $post->id]) }}"  method="post" enctype="multipart/form-data">
        {{ method_field('PUT') }}
        @csrf

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @if(!empty($errors->first()))
                <div class="alert alert-danger">
                    <strong>Diqqət!</strong> {{ $errors->first() }}.
                </div>
            @endif
            <div class="card">
                <div class="header">
                    <h2>
                        Adı
                    </h2>
                </div>
                <div class="body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                        <li role="presentation" class="active"><a href="#rusname" data-toggle="tab">Russian</a></li>
                        <li role="presentation"><a href="#spnname" data-toggle="tab">Azerbaijan</a></li>
                    </ul>
                    <br>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="rusname">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="name_ru" id="title" class="form-control" value="{{ $post->name_ru }}" required>
                                    <label class="form-label">Title Rus</label>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="spnname">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="name_az" id="title" class="form-control" value="{{ $post->name_az }}" required>
                                    <label class="form-label">Title Aze</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Text
                    </h2>
                </div>
                <div class="body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                        <li role="presentation" class="active"><a href="#rus" data-toggle="tab">Russian</a></li>
                        <li role="presentation"><a href="#spn" data-toggle="tab">Azerbaijan</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="rus">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <textarea name="text_ru" class="textru my-editor" id="tinymce" cols="30" rows="10" required>{{ $post->text_ru }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="spn">
                            <div class="form-group form-float">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <textarea name="text_az" class="textsp my-editor" id="tinymce" cols="30" rows="10" required>{{ $post->text_az }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Keywords ( Açar sözlər )
                    </h2>
                </div>
                <div class="body">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" name="keywords" class="form-control" value="{{ $post->keywords }}">
                            <label class="form-label">Keywords</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="card">
                <div class="header">
                    <h2>Category</h2>
                </div>
                <div class="body">
                    <div class="input-group ">
                        <div class="form-line">
                            <select name="category_id" data-live-search="true" class="bootstrap-select show-tick" data-live-search="true" required>
                                @foreach($categories as $category)
                                    <option {{($post->category_id == $category->id) ? "selected" : ""}} value="{{$category->id}}">{{$category->name_az}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="card">
                <div class="header">
                    <h2>Tag</h2>
                </div>
                <div class="body">
                    <div class="input-group">
                        <div class="form-line">
                            <select name="tags[]" class="show-tick" data-live-search="true" multiple data-selected-text-format="count">
                                @foreach($tags as $tag)
                                    <option {{ in_array($tag->id, $post->tags->pluck('id')->toArray()) ? 'selected' : ''}} value="{{$tag->id}}">{{$tag->name_az}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="form-group form-float">
                <div class="form-line">
                    <input name="image" type="file"  class="form-control hidden post-image" id="postImage" multiple>
                    <label for="postImage" style="cursor: pointer">
                        <img class="img-responsive thumbnail post-img-preview" src="{{asset('/images/posts/'.$post->image)}}">
                    </label>
                </div>
            </div>
        </div>

        <br>
        <div class="col-sm-12">
            <div class="demo-switch">
                <button type="submit" class="btn btn-primary waves-effect right">Edit post</button>
            </div>
        </div>

        <div class="row clearfix">

        </div>
    </form>
@endcomponent