@component('admin.components.form', $settings)
    <form action="{{ route('about.update',['about' => $about->id]) }}"  method="post" enctype="multipart/form-data">
        {{ method_field('PUT') }}
        @csrf
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                @if(!empty($errors->first()))
                    <div class="alert alert-danger">
                        <strong>Diqqət!</strong> {{ $errors->first() }}.
                    </div>
                @endif
                <div class="card">
                    <div class="header">
                        <h2>
                            Title
                        </h2>
                    </div>
                    <div class="body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs tab-nav-right" role="tablist">
                            <li role="presentation" class="active"><a href="#engtitle" data-toggle="tab">English</a></li>
                            <li role="presentation"><a href="#rustitle" data-toggle="tab">Russian</a></li>
                            <li role="presentation"><a href="#spntitle" data-toggle="tab">Azerbaijan</a></li>
                        </ul>
                        <br>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="engtitle">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="title_en" id="title" class="form-control" value="{{$about->title_en}}">
                                        <label class="form-label">Title Eng</label>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="rustitle">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="title_ru" id="title" class="form-control" value="{{$about->title_ru}}">
                                        <label class="form-label">Title Rus</label>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="spntitle">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="title_az" id="title" class="form-control" value="{{$about->title_az}}">
                                        <label class="form-label">Title Aze</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Text
                        </h2>
                    </div>
                    <div class="body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs tab-nav-right" role="tablist">
                            <li role="presentation" class="active"><a href="#eng" data-toggle="tab">English</a></li>
                            <li role="presentation"><a href="#rus" data-toggle="tab">Russian</a></li>
                            <li role="presentation"><a href="#spn" data-toggle="tab">Azerbaijan</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="eng">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <textarea name="text_en" class="texten" id="tinymce"  cols="30" rows="10">{{ $about->text_en }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="rus">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <textarea name="text_ru" class="textru" id="tinymce" cols="30" rows="10">{{ $about->text_ru }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="spn">
                                <div class="form-group form-float">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <textarea name="text_az" class="textsp" id="tinymce" cols="30" rows="10">{{ $about->text_az }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Video (link)</h2>
                        </div>
                        <div class="body">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input name="video" type="text"  class="form-control" value="{{$about->video}}">
                                    <label class="form-label">Video</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-sm-12">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input name="img" type="file" class="form-control hidden post-image" id="postImage">
                        <label for="postImage" style="cursor: pointer">
                            <img class="img-responsive thumbnail post-img-preview" src="{{asset('images/about/'.$about->image)}}">
                        </label>
                    </div>
                </div>
            </div>

        <br>
        <div class="clearfix demo-button-sizes">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <button type="submit" class="btn btn-primary m-t-15 waves-effect right">Update</button>
            </div>
        </div>
    </form>
    @endcomponent