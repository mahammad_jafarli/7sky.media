@component('admin.components.table', $settings)
<tr>
    <td>{{ $about->id }}</td>
    <td>{{ $about->title_az }}</td>
    <td>{!! $about->text_az !!}</td>
    <td><img src="{{ asset('/images/about/'.$about->image) }}" class="img-responsive" alt=""></td>
    <td>
        <a href="{{ route('about.edit',['about' => $about->id]) }}" class="btn btn-danger btn-circle waves-effect waves-circle waves-float"><i class="material-icons">edit</i></a>
    </td>
</tr>
@endcomponent