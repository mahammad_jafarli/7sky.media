@component('admin.components.form',$setting)
    {{-- Component content--}}
    <form id="form_validation" action="{{route('main_category.update',['main_category' => $mainCategory->id])}}" method="post" enctype="multipart/form-data">
        {{method_field('PUT')}}
        @csrf
        <div class="row clearfix">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                @if(!empty($errors->first()))
                    <div class="alert alert-danger">
                        <strong>Diqqət!</strong> {{ $errors->first() }}.
                    </div>
                @endif
                <div class="card">
                    <div class="header">
                        <h2>
                            Adı
                        </h2>
                    </div>
                    <div class="body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs tab-nav-right" role="tablist">
                            <li role="presentation" class="active"><a href="#rusname" data-toggle="tab">Russian</a></li>
                            <li role="presentation"><a href="#spnname" data-toggle="tab">Azerbaijan</a></li>
                        </ul>
                        <br>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="rusname">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="name_ru" id="title" class="form-control" value="{{ $mainCategory->name_ru }}">
                                        <label class="form-label">Ad Rus</label>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="spnname">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="name_az" id="title" class="form-control" value="{{ $mainCategory->name_az }}">
                                        <label class="form-label">Ad Aze</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="col-sm-12">
                <div class="demo-switch">
                    <button type="submit" class="btn btn-success waves-effect right">Əsas kateqoriyanı yenilə</button>
                </div>
            </div>
        </div>
    </form>
@endcomponent