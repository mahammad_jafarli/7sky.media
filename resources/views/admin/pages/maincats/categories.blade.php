@component('admin.components.table',$settings)
    {{-- Component content--}}
    @foreach($mainCategory as $category)
            <tr>
                <td>{{$category->id}}</td>
                <td>{{$category->name_az}}</td>
                <td>
                    <a class="btn bg-blue btn-circle waves-effect waves-circle waves-float" style="float: left;margin-right: 10px;" href="{{route('main_category.edit',['main_category' => $category->id])}}"><i class="material-icons">edit</i></a>
                    <form action="{{route('main_category.destroy',['main_category' => $category->id])}}" method="post">
                        {{ method_field('delete') }}
                        @csrf
                        <button type="submit" class="btn bg-red btn-circle waves-effect waves-circle waves-float">
                            <i class="material-icons">delete</i>
                        </button>
                    </form>
                </td>
            </tr>

    @endforeach
@endcomponent