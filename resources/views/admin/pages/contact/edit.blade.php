@component('admin.components.form',$setting)
    {{-- Component content--}}
    <form id="form_validation" action="{{route('contact.update',['contact' => $contact->id])}}" method="post">
        {{method_field('PUT')}}
        @csrf
        <div class="row clearfix">

            @if(!empty($errors->first()))
                <div class="alert alert-danger">
                    <strong>Diqqət!</strong> {{ $errors->first() }}.
                </div>
            @endif
            <div class="col-sm-6">
                <div class="input-group ">
                    <div class="form-line">
                        <input type="text" class="form-control" name="address" value="{{$contact->address}}">
                    </div>
                    <label class="form-label">Ünvan</label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="input-group ">
                    <div class="form-line">
                        <input type="text" class="form-control" name="location" value="{{$contact->location}}">
                    </div>
                    <label class="form-label">Xəritə</label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="input-group ">
                    <div class="form-line">
                        <input type="text" class="form-control" name="phone" value="{{$contact->phone}}">
                    </div>
                    <label class="form-label">Telefon</label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="input-group ">
                    <div class="form-line">
                        <input type="email" class="form-control" required name="email" value="{{$contact->email}}">
                    </div>
                    <label class="form-label">Email</label>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="demo-switch">
                    <button type="submit" class="btn btn-success waves-effect right">Yenilə</button>
                </div>
            </div>
        </div>
    </form>
@endcomponent