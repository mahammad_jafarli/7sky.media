@component('admin.components.table',$settings)
    {{-- Component content--}}
        <tr>
            <td>{{$contact->id}}</td>

            <td>{{$contact->address}}</td>
            <td>{{$contact->phone}}</td>
            <td>{{$contact->email}}</td>

            <td>
                <a class="btn bg-blue btn-circle waves-effect waves-circle waves-float" href="{{route('contact.edit',['contact' => $contact->id])}}"><i class="material-icons">edit</i></a>
            </td>
        </tr>

@endcomponent