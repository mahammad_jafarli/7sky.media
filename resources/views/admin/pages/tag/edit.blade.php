@component('admin.components.form',$setting)
    {{-- Component content--}}
    <form id="form_validation" action="{{route('tag.update',['tag' => $tag->id])}}" method="post">
        {{method_field('PUT')}}
        @csrf
        <div class="row clearfix">
            @if(!empty($errors->first()))
                <div class="alert alert-danger">
                    <strong>Diqqət!</strong> {{ $errors->first() }}.
                </div>
            @endif
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Ad
                            </h2>
                        </div>
                        <div class="body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                <li role="presentation" class="active"><a href="#engname" data-toggle="tab">English</a></li>
                                <li role="presentation"><a href="#rusname" data-toggle="tab">Russian</a></li>
                                <li role="presentation"><a href="#spnname" data-toggle="tab">Azerbaijan</a></li>
                            </ul>
                            <br>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="engname">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" name="name_en" id="title" class="form-control" value="{{ $tag->name_en }}">
                                            <label class="form-label">Name Eng</label>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="rusname">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" name="name_ru" id="title" class="form-control" value="{{ $tag->name_ru }}">
                                            <label class="form-label">Name Rus</label>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="spnname">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" name="name_az" id="title" class="form-control" value="{{ $tag->name_az }}">
                                            <label class="form-label">Name Aze</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="demo-switch">
                    <button type="submit" class="btn btn-success waves-effect right">Yenilə</button>
                </div>
            </div>
        </div>
    </form>
@endcomponent