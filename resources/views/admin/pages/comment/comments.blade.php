@component('admin.components.table',$settings)
    {{-- Component content--}}
    @foreach($comments as $comment)
        <tr>
            <td>{{$comment->id}}</td>
            <td>{{$comment->name}}</td>
            <td>{{$comment->email}}</td>
            <td>{{$comment->comment}}</td>
            <td>
                <a href="{{route('post.show',['post' => $comment->post->id])}}">{{$comment->post->name_ru}}</a>
            </td>
            <td>
               {{--  <a class="btn bg-blue btn-circle waves-effect waves-circle waves-float" href="{{route('post.edit',['post' => $post->id])}}"><i class="material-icons">edit</i></a> --}}
                <form action="{{route('comment.destroy',['comment' => $comment->id])}}" method="post">
                    {{ method_field('delete') }}
                    @csrf
                    <button type="submit" class="btn bg-red btn-circle waves-effect waves-circle waves-float">
                        <i class="material-icons">delete</i>
                    </button>
                </form>
            </td>
        </tr>
    @endforeach
@endcomponent