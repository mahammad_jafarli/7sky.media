@component('admin.components.form',$setting)
    {{-- Component content--}}
    <form id="form_validation" action="{{route('socials.update',['social' => $social->id])}}" method="post">
        {{method_field('PUT')}}
        @csrf
        <div class="row clearfix">
            @if(!empty($errors->first()))
                <div class="alert alert-danger">
                    <strong>Diqqət!</strong> {{ $errors->first() }}.
                </div>
            @endif
            <div class="col-sm-12">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input name="link" type="text" required class="form-control" value="{{$social->link}}">
                        <label class="form-label">Keçid</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input name="icon" type="text" required class="form-control" value="{{$social->icon}}">
                        <label class="form-label">İcon</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="demo-switch">
                    <button type="submit" class="btn btn-success waves-effect right">Yenilə</button>
                </div>
            </div>
        </div>
    </form>
@endcomponent