@component('admin.components.form',$setting)
    {{-- Component content--}}
    <form id="form_validation" action="{{route('socials.store')}}" method="post">
        @csrf
        <div class="row clearfix">
            @if(!empty($errors->first()))
                <div class="alert alert-danger">
                    <strong>Diqqət!</strong> {{ $errors->first() }}.
                </div>
            @endif
            <div class="col-sm-12">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input name="link" type="text" required class="form-control">
                        <label class="form-label">Keçid</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input name="icon" type="text" required class="form-control">
                        <label class="form-label">İcon</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <button type="submit" class="btn btn-success waves-effect right">Əlavə et</button>
            </div>
        </div>
        </div>
    </form>
@endcomponent