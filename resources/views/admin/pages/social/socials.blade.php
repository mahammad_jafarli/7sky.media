@component('admin.components.table',$settings)
    {{-- Component content--}}
    @foreach($socials as $social)
        <tr>
            <td>{{$social->id}}</td>
            <td>{{$social->link}}</td>
            <td>{{$social->icon}}</td>
            <td>
                <a class="btn bg-blue btn-circle waves-effect waves-circle waves-float" style="float: left;" href="{{route('socials.edit',['social' => $social->id])}}"><i class="material-icons">edit</i></a>
                <form action="{{route('socials.destroy',['social' => $social->id])}}" method="post">
                    {{ method_field('delete') }}
                    @csrf
                    <button type="submit" class="btn bg-red btn-circle waves-effect waves-circle waves-float">
                        <i class="material-icons">delete</i>
                    </button>
                </form>
            </td>
        </tr>
    @endforeach
@endcomponent