@component('admin.components.table',$settings)
    {{-- Component content--}}
    @foreach($users as $user)

        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->balance }}</td>
            <td>
                <div class="switch">
                    <label><input class="statusCheckBoxUser" data-row="{{$user->id}}" type="checkbox" {{($user->status) ? 'checked' : ""}}><span class="lever"></span></label>
                </div>
            </td>
            <td>
                <a class="btn bg-blue btn-circle waves-effect waves-circle waves-float" style="float: left;margin-right: 10px;" href="{{route('user.edit',['user' => $user->id])}}"><i class="material-icons">edit</i></a>
                <form action="{{route('user.destroy',['user' => $user->id])}}" method="post">
                    {{ method_field('delete') }}
                    @csrf
                    <button type="submit" class="btn bg-red btn-circle waves-effect waves-circle waves-float">
                        <i class="material-icons">delete</i>
                    </button>
                </form>
            </td>
        </tr>

    @endforeach
@endcomponent