@component('admin.components.form',$setting)
    {{-- Component content--}}
    <form id="form_validation" action="{{route('user.update',['user' => $user->id])}}" method="post">
        {{method_field('PUT')}}
        @csrf
        <div class="row clearfix">
            <div class="col-sm-12">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input name="name" type="text" required class="form-control" value="{{$user->name}}">
                        <label class="form-label">Ad</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input name="email" type="email" required class="form-control" value="{{$user->email}}">
                        <label class="form-label">Email</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input name="password" type="password" required class="form-control" placeholder="Password">
                        <label class="form-label">Şifrə</label>
                    </div>
                </div>
            </div>
            {{--<div class="col-sm-12">--}}
                {{--<div class="form-group form-float">--}}
                    {{--<div class="form-line">--}}
                        {{--<input name="password_confirmation" type="text" required class="form-control" placeholder="Confirm passswprd">--}}
                        {{--<label class="form-label">Təkrar şifrə</label>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="col-sm-12">
                <div class="demo-switch">
                    <button type="submit" class="btn btn-success waves-effect right">Yenilə</button>
                </div>
            </div>
        </div>
    </form>

    <script>
        $('.datepicker').bootstrapMaterialDatePicker({
            format: 'dddd DD MMMM YYYY',
            clearButton: true,
            weekStart: 1,
            time: false
        });
    </script>
@endcomponent