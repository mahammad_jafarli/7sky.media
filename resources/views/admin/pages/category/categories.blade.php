@component('admin.components.table',$settings)
    {{-- Component content--}}
    @foreach($categories as $category)
        @if(!$category->parent_id == 0)
            <tr>
                <td>{{$category->id}}</td>
                <td>{{$category->name_az}}</td>
                {{--<td>--}}
                    {{--@if($category->parent_id == 0)--}}
                        {{--@foreach($category->parent_category as $parent)--}}
                            {{--{{ $parent->name_az }}--}}
                        {{--@endforeach--}}
                    {{--@endif--}}
                {{--</td>--}}
                <td>
                    <a class="btn bg-blue btn-circle waves-effect waves-circle waves-float" style="float: left;margin-right: 10px;" href="{{route('category.edit',['category' => $category->id])}}"><i class="material-icons">edit</i></a>
                    <form action="{{route('category.destroy',['category' => $category->id])}}" method="post">
                        {{ method_field('delete') }}
                        @csrf
                        <button type="submit" class="btn bg-red btn-circle waves-effect waves-circle waves-float">
                            <i class="material-icons">delete</i>
                        </button>
                    </form>
                </td>
            </tr>
        @endif
    @endforeach
@endcomponent