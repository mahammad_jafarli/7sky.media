<aside id="leftsidebar" class="sidebar">
    <!-- User Info -->
    <div class="user-info">
        <div class="image">
            <img src="{{asset('admin/images/user.png')}}" width="48" height="48" alt="User" />
        </div>
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{auth()->user()->name }}
            </div>
            <div class="email">
                {{auth()->user()->email}}
            </div>
            <div class="btn-group user-helper-dropdown">
                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                <ul class="dropdown-menu pull-right">
                    <li>
                        <a  href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
        <ul class="list ml-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="{{request()->routeIs('user.index') ? 'active' : ''}}">
                <a href="{{route('user.index')}}">Пользователей</a>
            </li>
            <li class="{{request()->routeIs('main_category.index') ? 'active' : ''}}">
                <a href="{{route('main_category.index')}}">Основные категории</a>
            </li>
            <li class="{{request()->routeIs('category.index') ? 'active' : ''}}">
                <a href="{{route('category.index')}}">Категории</a>
            </li>
            <li class="{{request()->routeIs('tag.index') ? 'active' : ''}}">
                <a href="{{route('tag.index')}}">Тег</a>
            </li>
            <li class="{{request()->routeIs('post.index') ? 'active' : ''}}">
                <a href="{{route('post.index')}}">Новости</a>
            </li>
            <li class="{{request()->routeIs('comment.index') ? 'active' : ''}}">
                <a href="{{route('comment.index')}}">Комментарии</a>
            </li>
            <li class="{{request()->routeIs('about.index') ? 'active' : ''}}">
                <a href="{{route('about.index')}}">О нас</a>
            </li>
        
            <li class="{{request()->routeIs('socials.index') ? 'active' : ''}}">
                <a href="{{route('socials.index')}}">Социальные сети</a>
            </li>
            <li class="{{request()->routeIs('contact.index') ? 'active' : ''}}">
                <a href="{{route('contact.index')}}">Контакт</a>
            </li>
        </ul>
    </div>
    <!-- #Menu -->
    <!-- Footer -->
    <div class="legal">
        <div class="copyright">
            &copy; 2018 - 2019 <a href="javascript:void(0);">7sky.media | Admin Panel</a>.
        </div>
        <div class="version">
            <b>By: </b> <a href="http://jafarli.me/" target="_blank">Jafarli.me</a>.
        </div>
    </div>
    <!-- #Footer -->
</aside>