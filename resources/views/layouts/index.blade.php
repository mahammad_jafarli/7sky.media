<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    @include('frontend.includes.header')
</head>
<body>
<div class="spinner-cover">
    <div class="spinner-inner">
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
    </div>
</div>
<div class="spinner-cover">
    <div class="spinner-inner">
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
    </div>
</div>
<div id="wrapper">

    @include('frontend.includes.leftSidebar')

    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="container">
               @include('frontend.includes.topNavbar')
            </div>
        </div>
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-12 header">
                        <h1 class="logo"><a href="{{ url('/') }}">7 SKY</a></h1>
                        <p class="tagline">NEWSPAPER / MAGAZINE / PUBLISHER</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-nav section_margin">
            <div class="container-fluid">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-12 main_nav_cover" id="nav">
                            @include('frontend.includes.navbar')
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @yield('breadcrumb')
        <div class="container-fluid">
            <div class="container">
                <div class="primary margin-15">
                    <div class="row">
                        <div class="col-md-8">
                            @yield('content')
                        </div>
                        <!--Start Sidebar-->
                        @include('frontend.includes.rightSidebar')
                        <!--End Sidebar-->
                    </div>
                </div> <!--.primary-->

            </div>
        </div>

        @include('frontend.includes.footer')

        <div class="gototop js-top">
            <a href="#" class="js-gotop"><span></span></a>
        </div>
    </div> <!--page-content-wrapper-->
    <script src="{{ url('https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.slicknav.min.js') }}"></script>
    <script src="{{ asset('assets/js/masonry.pkgd.min.js') }}"></script>
    <!-- Main -->
    <script src="{{ asset('assets/js/main.js') }}"></script>
    <script src="{{ asset('assets/js/smart-sticky.js') }}"></script>
    <script src="{{ asset('assets/js/theia-sticky-sidebar.js') }}"></script>
    <script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
    @stack('scripts')
</div> <!--#wrapper-->
</body>
</html>