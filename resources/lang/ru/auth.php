<?php

return [

    'login' => 'Вход',
    'remember' => ' Запомнить меня',
    'reset' => 'Забыли пароль?',
    'name' => 'Имя Фамилия',
    'email' => 'Email-адрес',
    'password' => 'Пароль',
    'confirm' => 'Подтвердите пароль',
    'register' => 'Создать новый аккаунт...',
    'registerBtn' => 'Регистрация',
    'resetTitle' => 'Восстановление забытого пароля',
    'resetBtn' => 'Отправить',
    'update' => 'Обновить',
    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

];
