<?php

return [

    'login' => 'Daxil ol',
    'remember' => 'Yadda saxla',
    'reset' => 'Şifrəmi unutdum?',
    'name' => 'Tam Adınız',
    'email' => 'E-poçt ünvanı',
    'password' => 'Şifrə',
    'confirm' => 'Şifrəni təsdiqlə',
    'register' => 'Yeni müştəri hesabınızı indi aktiv edin . . .',
    'registerBtn' => 'Qeydiyyat',
    'resetTitle' => 'İtirilmiş şifrə yenilənməsi',
    'resetBtn' => 'Təsdiqlə',
    'update' => 'Yenilə',
    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

];
