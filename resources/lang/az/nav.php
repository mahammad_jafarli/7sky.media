<?php

return [

    'login' => 'Daxil ol',
    'signin' => 'Qeydiyyatdan keç',
    'search' => 'Axdar',
    'trending' => 'Ən çox oxunanlar',
    'latest' => 'Ən son əlavə olunanlar',
    'user' => 'Müştəri Paneli',
    'logout' => 'Çıxış',
    'more' => 'Daha çox',
];
