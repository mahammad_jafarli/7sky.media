<?php

return [

    'subscribe' => 'Abunə olun',
    'subscribeBtn' => 'Abunə olun',
    'tags' => 'Teqlər',
    'mail' => 'Elektron poçtunuz',
    'name' => 'Ad',
    'comment' => 'Şərh',
    'comments' => 'Şərhlər',
    'commentWrite' => 'Şərh yaz',
    'send' => 'Göndər',
    'commentDesc' => 'E-poçt ünvanınız dərc olunmayacaq',
    'view' => 'Baxış',
    'similar' => 'Oxşar xəbərlər',
];
