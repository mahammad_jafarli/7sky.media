<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Frontend\IndexController@index');
Route::post('/postSubscribe', 'Frontend\IndexController@subscribe');
Route::get('{post}-{name}', 'Frontend\IndexController@show');
Route::get('/category/{id}-{name}', 'Frontend\IndexController@category');
Route::post('/commentPost', 'Frontend\IndexController@commentPost');
Route::get('/search', 'Frontend\IndexController@search');
Route::get('/lang/{lang}', 'LangController@index');

Auth::routes();

Route::get('/home', 'Frontend\IndexController@index')->name('home');
//set status routes
Route::post('set-statususer', 'Backend\UsersController@setStatus')->name('setStatusUser');
Route::get('/admin/login', 'Auth\AdminLoginController@showLoginForm');
Route::post('admin/login', 'Auth\AdminLoginController@login')->name('admin.login');

Route::group(['middleware' => 'banned','auth'], function(){
    Route::get('/user', 'Frontend\UserController@index');
});

Route::group(['prefix' => 'admin','middleware' => 'assign.guard:admin,admin/login'],function(){
    Route::get('/home', 'Backend\AdminController@index');
    Route::get('/getSubCategory/{categoryId}', 'Backend\PostsController@getSubCategoryById');
    Route::resources([
        'user' => 'Backend\UsersController',
        'category' => 'Backend\CategoriesController',
        'main_category' => 'Backend\MainCategoryController',
        'tag' => 'Backend\TagsController',
        'socials' => 'Backend\SocialsController',
        'contact' => 'Backend\ContactController',
        'about' => 'Backend\AboutController',
        'post' => 'Backend\PostsController',
        'comment' => 'Backend\CommentsController',
    ]);
});